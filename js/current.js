$(document).ready(function(){
    
    $("#submitCity").click(function(){
        return getWeather();
    });
    
    
});

function getWeather(){
    var city = $("#city").val();
    
    if(city != ''){
        
        $.ajax({
           url: 'http://api.openweathermap.org/data/2.5/weather?q=' + city + "&units=metric" + "&APPID=c10bb3bd22f90d636baa008b1529ee25",
            type: "GET",
            dataType: "jsonp",
            success: function(data){
                var widget1 = showResults1(data)
                var widget2 = showResults2(data)
                var widget3 = showResults3(data)
                $("#showWeather").html(widget1);
                $("#humidity-div").html(widget2);
                $("#city-name").html($("#city").val());
                $("#icon").html(widget3);
                $("#city").val('');
            }
            
            
        });	
        
        
    }else{
        // $("#error").html("<alert-box>City field cannot be empty</alert-box>");
        // $("#error").html("<div class='demo-alert-box'><strong>Ошибка!</strong><slot></slot></div>");
    }
    
    
}

function showResults1(data){
	return "<p>" + data.main.temp+"&deg;C</p>";		   
}

function showResults2(data){
	return "<p>"+data.main.humidity+"%</p>";		   
}

function showResults3(data){
	return "<img style='height:80%; width:80%;' src='http://openweathermap.org/img/w/"+data.weather[0].icon+".png'> ";		   
}

